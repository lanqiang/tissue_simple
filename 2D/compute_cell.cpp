#include "compute_cell.h"
#include "cell.h"

//#include "mkl_vsl.h"
#include <cmath>
#include <cstring>
#include <stdlib.h>

#include <iostream>
using namespace std;
void comp_cell(struct cell &Cell, int ix, int iy, int offset_x, int offset_y,
double t, double ist, int bcl, double **v,double **dtstep)
{
double nao = 140;
double ko = 5.4;
double cao = 1.8;
double iso = 0;
double gna = 75;
double mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs;
double mss,hss,jss,hssp;
double h,hp,finap;
double mlss,mltau,hlss,hlssp,hltau,hlptau;

double gnal,finalp;
double naiont,caiont,kiont,it,istim;
double ina,inal,INab,ilca,icab,icat,ipca,ilcana,INaK,inaca,inacass,Ito,IKr,IKs,IK1,IKb,ilcak;

double R = 8314;
double temp = 310;
double frdy = 96485;
double ena;

//buffers
double kmcsqn = 1;
double csqnbar = 10;
double bsrbar = 0.047;
double	bslbar = 1.124;
double	kmbsr = 0.00087;
double	kmbsl = 0.0087;
double trpnbar = 0.07;
double kmtrpn = 0.0005;
double cmdnbar = 0.05;
double kmcmdn = 0.00238;

//camkinase
double const aCaMK=0.05;
double const bCaMK=0.00068;
double const CaMKo=0.05;
double const KmCaM=0.0015;
double const KmCaMK=0.15;
double CaMKa,CaMKb;

double dt = dtstep[ix][iy];

gna = gna;
mtau = 1.0/(6.765*exp((v[ix][iy]+11.64)/34.77)+8.552*exp(-(v[ix][iy]+77.42)/5.955));
hftau=1.0/(1.432e-5*exp(-(v[ix][iy]+1.196)/6.285)+6.149*exp((v[ix][iy]+0.5096)/20.27));
hstau=1.0/(0.009794*exp(-(v[ix][iy]+17.95)/28.05)+0.3343*exp((v[ix][iy]+5.730)/56.66));
hsptau=3.0*hstau;
jtau=2.038+1.0/(0.02136*exp(-(v[ix][iy]+100.6)/8.281)+0.3052*exp((v[ix][iy]+0.9941)/38.45));
jptau=1.46*jtau;
Ahf=0.99;
Ahs=1.0-Ahf;
mss = 1.0/(1.0+exp((-(v[ix][iy]+44.57))/9.871));

if (Cell.soicr>0.001)
	mss = 1.0/(1.0+exp((-(v[ix][iy]+49.57))/9.871));

hss =1.0/(1+exp((v[ix][iy]+82.90)/6.086));
hssp=1.0/(1+exp((v[ix][iy]+89.1)/6.086));
if (iso == 1)
{		
	hss =1.0/(1+exp((v[ix][iy]+82.90+5)/6.086));             		
	hssp=1.0/(1+exp((v[ix][iy]+89.1+5)/6.086));
}
jss = hss;	

Cell.m = mss-(mss-Cell.m)*exp(-dt/mtau);
Cell.hf=hss-(hss-Cell.hf)*exp(-dt/hftau);
Cell.hs=hss-(hss-Cell.hs)*exp(-dt/hstau);
Cell.hsp=hssp-(hssp-Cell.hsp)*exp(-dt/hsptau);

h = Ahf*Cell.hf+Ahs*Cell.hs;
hp=Ahf*Cell.hf+Ahs*Cell.hsp;
Cell.j = jss-(jss-Cell.j)*exp(-dt/jtau);
Cell.jp=jss-(jss-Cell.jp)*exp(-dt/jptau);
	
finap=(1.0/(1.0+KmCaMK/CaMKa));

mlss=1.0/(1.0+exp((-(v[ix][iy]+42.85))/5.264));
mltau = mtau;    
Cell.ml=mlss-(mlss-Cell.ml)*exp(-dt/mltau);
    
hlss=1.0/(1.0+exp((v[ix][iy]+87.61)/7.488));
hlssp=1.0/(1.0+exp((v[ix][iy]+93.81)/7.488));
hltau=200.0;
hlptau=3.0*hltau;
Cell.hl=hlss-(hlss-Cell.hl)*exp(-dt/hltau);
Cell.hlp=hlssp-(hlssp-Cell.hlp)*exp(-dt/hlptau);
    
gnal = 0.0075;
finalp = finap;

ena = ((R*temp)/frdy)*log(nao/Cell.nai);
    	
ina=gna*(v[ix][iy]-ena)*Cell.m*Cell.m*Cell.m*((1.0-finap)*h*Cell.j+finap*hp*Cell.jp);
inal=gnal*(v[ix][iy]-ena)*Cell.ml*((1.0-finalp)*Cell.hl+finalp*Cell.hlp);	

double EK;
EK=(R*temp/frdy)*log(ko/Cell.ki);

double ass;     
ass=1.0/(1.0+exp((-(v[ix][iy]-14.34))/14.82));
double atau;
atau=1.0515/(1.0/(1.2089*(1.0+exp(-(v[ix][iy]-18.4099)/29.3814)))+3.5/(1.0+exp((v[ix][iy]+100.0)/29.3814))); 

Cell.a=ass-(ass-Cell.a)*exp(-dt/atau);

double iss;     
iss=1.0/(1.0+exp((v[ix][iy]+43.94)/5.711));
double delta_epi=1.0;
double iftau,istau;     

iftau=4.562+1/(0.3933*exp((-(v[ix][iy]+100.0))/100.0)+0.08004*exp((v[ix][iy]+50.0)/16.59));
istau=23.62+1/(0.001416*exp((-(v[ix][iy]+96.52))/59.05)+1.780e-8*exp((v[ix][iy]+114.1)/8.079));
     
iftau*=delta_epi;
istau*=delta_epi;

double AiF,AiS;     
AiF=1.0/(1.0+exp((v[ix][iy]-213.6)/151.2));
AiS=1.0-AiF;
     
Cell.iF=iss-(iss-Cell.iF)*exp(-dt/iftau);
Cell.iS=iss-(iss-Cell.iS)*exp(-dt/istau);

double i_ito;     
i_ito=AiF*Cell.iF+AiS*Cell.iS;
     
double assp;
assp=1.0/(1.0+exp((-(v[ix][iy]-24.34))/14.82));
Cell.ap=assp-(assp-Cell.ap)*exp(-dt/atau);
     
double dti_develop,dti_recover;
dti_develop=1.354+1.0e-4/(exp((v[ix][iy]-167.4)/15.89)+exp(-(v[ix][iy]-12.23)/0.2154));
dti_recover=1.0-0.5/(1.0+exp((v[ix][iy]+70.0)/20.0));
     
double tiFp,tiSp;
tiFp=dti_develop*dti_recover*iftau;
tiSp=dti_develop*dti_recover*istau;

Cell.iFp=iss-(iss-Cell.iFp)*exp(-dt/tiFp);
Cell.iSp=iss-(iss-Cell.iSp)*exp(-dt/tiSp);

double ip;     
ip=AiF*Cell.iFp+AiS*Cell.iSp;
     
double Gto=0.02;
double fItop;     
fItop=(1.0/(1.0+KmCaMK/CaMKa));

Ito=Gto*(v[ix][iy]-EK)*((1.0-fItop)*Cell.a*i_ito+fItop*Cell.ap*ip);

double xkb=1.0/(1.0+exp(-(v[ix][iy]-14.48)/18.34));
double GKb=0.003;
if (iso == 1) 
	 GKb = 2.5*GKb;

IKb=GKb*xkb*(v[ix][iy]-EK);

double PNab=3.75e-10;
double vffrt = v[ix][iy]*frdy*frdy/(R*temp);
double vfrt = v[ix][iy]*frdy/(R*temp);
INab=PNab*vffrt*(Cell.nai*exp(vfrt)-nao)/(exp(vfrt)-1.0);

double EKs=(R*temp/frdy)*log((ko+0.01833*nao)/(Cell.ki+0.01833*Cell.nai));
double xs1ss=1.0/(1.0+exp((-(v[ix][iy]+11.60))/8.932));
double txs1=817.3+1.0/(2.326e-4*exp((v[ix][iy]+48.28)/17.80)+0.001292*exp((-(v[ix][iy]+210.0))/230.0));
Cell.xs1=xs1ss-(xs1ss-Cell.xs1)*exp(-dt/txs1);
double xs2ss=xs1ss;
double txs2=1.0/(0.01*exp((v[ix][iy]-50.0)/20.0)+0.0193*exp((-(v[ix][iy]+66.54))/31.0));
Cell.xs2=xs2ss-(xs2ss-Cell.xs2)*exp(-dt/txs2);
double KsCa=1.0+0.6/(1.0+pow(3.8e-5/3.8e-5,1.4));
double GKs=0.0034;
if (iso == 1) GKs = 3.2*GKs;

IKs=GKs*KsCa*Cell.xs1*Cell.xs2*(v[ix][iy]-EKs);

double xrss=1.0/(1.0+exp((-(v[ix][iy]+8.337))/6.789));
double txrf=12.98+1.0/(0.3652*exp((v[ix][iy]-31.66)/3.869)+4.123e-5*exp((-(v[ix][iy]-47.78))/20.38));
double txrs=1.865+1.0/(0.06629*exp((v[ix][iy]-34.70)/7.355)+1.128e-5*exp((-(v[ix][iy]-29.74))/25.94));
double Axrf=1.0/(1.0+exp((v[ix][iy]+54.81)/38.21));
double Axrs=1.0-Axrf;
Cell.xrf=xrss-(xrss-Cell.xrf)*exp(-dt/txrf);
Cell.xrs=xrss-(xrss-Cell.xrs)*exp(-dt/txrs);
double xr=Axrf*Cell.xrf+Axrs*Cell.xrs;
double rkr=1.0/(1.0+exp((v[ix][iy]+55.0)/75.0))*1.0/(1.0+exp((v[ix][iy]-10.0)/30.0));
double GKr=0.046; 



IKr=GKr*sqrt(ko/5.4)*xr*rkr*(v[ix][iy]-EK);

double xk1ss=1.0/(1.0+exp(-(v[ix][iy]+2.5538*ko+144.59)/(1.5692*ko+3.8115)));
double txk1=122.2/(exp((-(v[ix][iy]+127.2))/20.36)+exp((v[ix][iy]+236.8)/69.33));
Cell.xk1=xk1ss-(xk1ss-Cell.xk1)*exp(-dt/txk1);
double rk1=1.0/(1.0+exp((v[ix][iy]+105.8-2.6*ko)/9.493));
double GK1=0.1908;

if (Cell.soicr>0.001)
	GK1 = 0.1*GK1;

IK1=GK1*sqrt(ko)*rk1*Cell.xk1*(v[ix][iy]-EK);

double zna = 1;
double zca = 2;
double k1p=949.5;
double k1m=182.4;
double k2p=687.2;
double k2m=39.4;
double k3m=79300.0;
double k3p;
double k4p;
double k4m=40.0;
double Knai0=9.073;
double Knao0=27.78;
double delta=-0.1550;
double Knai=Knai0*exp((delta*v[ix][iy]*frdy)/(3.0*R*temp));
if (iso == 1) Knai = 0.7*Knai;
double Knao=Knao0*exp(((1.0-delta)*v[ix][iy]*frdy)/(3.0*R*temp));
double Kki=0.5;
double Kko=0.3582;
double MgADP=0.05;
double MgATP=9.8;
double Kmgatp=1.698e-7;
double H=1.0e-7;
double eP=4.2;
double Khp=1.698e-7;
double Knap=224.0;
double Kxkur=292.0;
double P=eP/(1.0+H/Khp+Cell.nai/Knap+Cell.ki/Kxkur);
double a1=(k1p*pow(Cell.nai/Knai,3.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
double b1=k1m*MgADP;
double a2=k2p;
double b2=(k2m*pow(nao/Knao,3.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
double a3=(k3p*pow(ko/Kko,2.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
double b3=(k3m*P*H)/(1.0+MgATP/Kmgatp);
double a4=(k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
double b4=(k4m*pow(Cell.ki/Kki,2.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
double x1=a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
double x2=b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
double x3=a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
double x4=b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;
double E1=x1/(x1+x2+x3+x4);
double E2=x2/(x1+x2+x3+x4);
double E3=x3/(x1+x2+x3+x4);
double E4=x4/(x1+x2+x3+x4);
double zk=1.0;
double JnakNa=3.0*(E1*a3-E2*b3);
double JnakK=2.0*(E4*b1-E3*a1);
double Pnak=30;

INaK=Pnak*(zna*JnakNa+zk*JnakK);

double dss=1.0/(1.0+exp((-(v[ix][iy]+3.940))/4.230));
if (iso == 1) dss=1.0/(1.0+exp((-(v[ix][iy]+3.940+14))/4.230));

double td=0.6+1.0/(exp(-0.05*(v[ix][iy]+6.0))+exp(0.09*(v[ix][iy]+14.0)));
Cell.d=dss-(dss-Cell.d)*exp(-dt/td);

double fss=1.0/(1.0+exp((v[ix][iy]+19.58)/3.696));
if (iso == 1) fss=1.0/(1.0+exp((v[ix][iy]+19.58+8)/3.696));

double tff=7.0+1.0/(0.0045*exp(-(v[ix][iy]+20.0)/10.0)+0.0045*exp((v[ix][iy]+20.0)/10.0));
double tfs=1000.0+1.0/(0.000035*exp(-(v[ix][iy]+5.0)/4.0)+0.000035*exp((v[ix][iy]+5.0)/6.0));
double Aff=0.6;
double Afs=1.0-Aff;
Cell.ff=fss-(fss-Cell.ff)*exp(-dt/tff);
Cell.fs=fss-(fss-Cell.fs)*exp(-dt/tfs);
double f=Aff*Cell.ff+Afs*Cell.fs;
double fcass=fss;
double tfcaf=7.0+1.0/(0.04*exp(-(v[ix][iy]-4.0)/7.0)+0.04*exp((v[ix][iy]-4.0)/7.0));
double tfcas=100.0+1.0/(0.00012*exp(-v[ix][iy]/3.0)+0.00012*exp(v[ix][iy]/7.0));
double Afcaf=0.3+0.6/(1.0+exp((v[ix][iy]-10.0)/10.0));
double Afcas=1.0-Afcaf;
Cell.fcaf=fcass-(fcass-Cell.fcaf)*exp(-dt/tfcaf);
Cell.fcas=fcass-(fcass-Cell.fcas)*exp(-dt/tfcas);
double fca=Afcaf*Cell.fcaf+Afcas*Cell.fcas;
double tjca=75.0;
Cell.jca=fcass-(fcass-Cell.jca)*exp(-dt/tjca);
double tffp=2.5*tff;
Cell.ffp=fss-(fss-Cell.ffp)*exp(-dt/tffp);
double fp=Aff*Cell.ffp+Afs*Cell.fs;
double tfcafp=2.5*tfcaf;
Cell.fcafp=fcass-(fcass-Cell.fcafp)*exp(-dt/tfcafp);
double fcap=Afcaf*Cell.fcafp+Afcas*Cell.fcas;

//ICaL//
double Kmn=0.002;
double k2n=1000.0;
double km2n=Cell.jca*1.0;
double anca=1.0/(k2n/km2n+pow(1.0+Kmn/Cell.cass,4.0));
Cell.nca=anca*k2n/km2n-(anca*k2n/km2n-Cell.nca)*exp(-km2n*dt);
double fICaLp=(1.0/(1.0+KmCaMK/CaMKa));
double PCa=0.0001;
if (iso == 1) PCa = 2.5*PCa;
double PCap=1.1*PCa;
double PCaNa=0.00125*PCa;
double PCaK=3.574e-4*PCa;
double PCaNap=0.00125*PCap;
double PCaKp=3.574e-4*PCap;
double PhiCaL=4.0*vffrt*(1e-3*Cell.cass*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
double PhiCaNa=1.0*vffrt*(0.75*Cell.nass*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
double PhiCaK=1.0*vffrt*(0.75*Cell.kss*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);

 ilca=(1.0-fICaLp)*PCa*PhiCaL*Cell.d*(f*(1.0-Cell.nca)+Cell.jca*fca*Cell.nca)+fICaLp*PCap*PhiCaL*Cell.d*(fp*(1.0-Cell.nca)+Cell.jca*fcap*Cell.nca);
 ilcana=(1.0-fICaLp)*PCaNa*PhiCaNa*Cell.d*(f*(1.0-Cell.nca)+Cell.jca*fca*Cell.nca)+fICaLp*PCaNap*PhiCaNa*Cell.d*(fp*(1.0-Cell.nca)+Cell.jca*fcap*Cell.nca);
 ilcak=(1.0-fICaLp)*PCaK*PhiCaK*Cell.d*(f*(1.0-Cell.nca)+Cell.jca*fca*Cell.nca)+fICaLp*PCaKp*PhiCaK*Cell.d*(fp*(1.0-Cell.nca)+Cell.jca*fcap*Cell.nca);

double ar = 0.0011;
double	al = 0.01;
double	pi = 3.142;
double	ageo = 2*pi*ar*ar + 2*pi*ar*al;
double	acap = 2*ageo;
double vcell = 1000*pi*ar*ar*al;	
double	vmyo = 0.68*vcell;

double	vnsr = 0.0552*vcell;
double	vjsr = 0.0048*vcell;
double	vss = 0.02*vcell;


// calcium currents
double kna1=15.0;
double kna2=5.0;
double kna3=88.12;
double kasymm=12.5;
double wna=6.0e4;
double wca=6.0e4;
double wnaca=5.0e3;
double kcaon=1.5e6;
double kcaoff=5.0e3;
double qna=0.5224;
double qca=0.1670;
double hca=exp((qca*v[ix][iy]*frdy)/(R*temp));
double hna=exp((qna*v[ix][iy]*frdy)/(R*temp));
double h1=1+Cell.nai/kna3*(1+hna);
double h2=(Cell.nai*hna)/(kna3*h1);
double h3=1.0/h1;
double h4=1.0+Cell.nai/kna1*(1+Cell.nai/kna2);
double h5=Cell.nai*Cell.nai/(h4*kna1*kna2);
double h6=1.0/h4;
double h7=1.0+nao/kna3*(1.0+1.0/hna);
double h8=nao/(kna3*hna*h7);
double h9=1.0/h7;
double h10=kasymm+1.0+nao/kna1*(1.0+nao/kna2);
double h11=nao*nao/(h10*kna1*kna2);
double h12=1.0/h10;
double k1=h12*cao*kcaon;
double k2=kcaoff;
k3p=h9*wca;
double k3pp=h8*wnaca;
double k3=k3p+k3pp;
k4p=h3*wca/hca;
double k4pp=h2*wnaca;
double k4=k4p+k4pp;
double k5=kcaoff;
double k6=h6*Cell.cai*kcaon;
double k7=h5*h2*wna;
double k8=h8*h11*wna;
x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
E1=x1/(x1+x2+x3+x4);
E2=x2/(x1+x2+x3+x4);
E3=x3/(x1+x2+x3+x4);
E4=x4/(x1+x2+x3+x4);
double KmCaAct=150.0e-6;
double allo=1.0/(1.0+pow(KmCaAct/Cell.cai,2.0));

double JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
double JncxCa=E2*k2-E1*k1;
double Gncx=0.0008;

if (Cell.soicr>0)
	Gncx = 1.5*Gncx;

inaca=0.8*Gncx*allo*(zna*JncxNa+zca*JncxCa);

h1=1+Cell.nai/kna3*(1+hna);
h2=(Cell.nai*hna)/(kna3*h1);
h3=1.0/h1;
h4=1.0+Cell.nai/kna1*(1+Cell.nai/kna2);
h5=Cell.nai*Cell.nai/(h4*kna1*kna2);
h6=1.0/h4;
h7=1.0+nao/kna3*(1.0+1.0/hna);
h8=nao/(kna3*hna*h7);
h9=1.0/h7;
h10=kasymm+1.0+nao/kna1*(1+nao/kna2);
h11=nao*nao/(h10*kna1*kna2);
h12=1.0/h10;
k1=h12*cao*kcaon;
k2=kcaoff;
k3p=h9*wca;
k3pp=h8*wnaca;
k3=k3p+k3pp;
k4p=h3*wca/hca;
k4pp=h2*wnaca;
k4=k4p+k4pp;
k5=kcaoff;
k6=h6*Cell.cass*kcaon;
k7=h5*h2*wna;
k8=h8*h11*wna;
x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
E1=x1/(x1+x2+x3+x4);
E2=x2/(x1+x2+x3+x4);
E3=x3/(x1+x2+x3+x4);
E4=x4/(x1+x2+x3+x4);
KmCaAct=150.0e-6;
allo=1.0/(1.0+pow(KmCaAct/Cell.cass,2.0));
JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
JncxCa=E2*k2-E1*k1;

inacass=0.2*Gncx*allo*(zna*JncxNa+zca*JncxCa);

double PCab=2.5e-8;
icab=PCab*4.0*vffrt*(Cell.cai*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
double GpCa=0.0005;
ipca=GpCa*Cell.cai/(0.0005+Cell.cai);

CaMKb=CaMKo*(1.0-Cell.CaMKt)/(1.0+KmCaM/Cell.cass);
CaMKa=CaMKb+Cell.CaMKt;
Cell.CaMKt+=dt*(aCaMK*CaMKb*(CaMKb+Cell.CaMKt)-bCaMK*Cell.CaMKt);

double JdiffNa=(Cell.nass-Cell.nai)/2.0;
double JdiffK=(Cell.kss-Cell.ki)/2.0;
double Jdiff=(Cell.cass-Cell.cai)/0.2;

double bt=4.75;
double a_rel=0.5*bt;
double Jrel_inf=a_rel*(-ilca)/(1.0+pow(1.5/Cell.cajsr,8.0));

double tau_rel=bt/(1.0+0.0123/Cell.cajsr);
if (tau_rel<0.005)
{
tau_rel=0.005;
}

Cell.Jrelnp=Jrel_inf-(Jrel_inf-Cell.Jrelnp)*exp(-dt/tau_rel);

double btp=1.25*bt;
double a_relp=0.5*btp;
double Jrel_infp=a_relp*(-ilca)/(1.0+pow(1.5/Cell.cajsr,8.0));

double tau_relp=btp/(1.0+0.0123/Cell.cajsr);
if (tau_relp<0.005)
{
tau_relp=0.005;
}
Cell.Jrelp=Jrel_infp-(Jrel_infp-Cell.Jrelp)*exp(-dt/tau_relp);
double fJrelp=(1.0/(1.0+KmCaMK/CaMKa));

//soicr

	if (Cell.cajsr > 1.) {Cell.soicr = 1;}


double soicr_tau = 10;
Cell.soicr = Cell.soicr*exp(-dt/soicr_tau);

double Jrel=(1.0-fJrelp)*Cell.Jrelnp+fJrelp*Cell.Jrelp + 0.5*Cell.soicr;

double Jupnp=0.004375*Cell.cai/(Cell.cai+0.00092);
double Jupp=2.75*0.004375*Cell.cai/(Cell.cai+0.00092-0.00017);

double fJupp=(1.0/(1.0+KmCaMK/CaMKa));
double Jleak=0.0039375*Cell.cansr/15.0;
double Jup=(1.0-fJupp)*Jupnp+fJupp*Jupp-Jleak;


double Jtr=(Cell.cansr-Cell.cajsr)/100.0;
double dnai = dt*(-(ina+inal+3.0*inaca+3.0*INaK+INab)*acap/(frdy*vmyo)+JdiffNa*vss/vmyo);

Cell.nai = dnai + Cell.nai;
Cell.nass+=dt*(-(ilcana+3.0*inacass)*acap/(frdy*vss)-JdiffNa);

Cell.ki+=dt*(-(Ito+IKr+IKs+IK1+IKb+istim-2.0*INaK)*acap/(frdy*vmyo)+JdiffK*vss/vmyo);
Cell.kss+=dt*(-(ilcak)*acap/(frdy*vss)-JdiffK);

double Bcai=1.0/(1.0+cmdnbar*kmcmdn/pow(kmcmdn+Cell.cai,2.0)+trpnbar*kmtrpn/pow(kmtrpn+Cell.cai,2.0));
Cell.cai+=dt*(Bcai*(-(ipca+icab-2.0*inaca)*acap/(2.0*frdy*vmyo)-Jup*vnsr/vmyo+Jdiff*vss/vmyo));

double Bcass=1.0/(1.0+bsrbar*kmbsr/pow(kmbsr+Cell.cass,2.0)+bslbar*kmbsl/pow(kmbsl+Cell.cass,2.0));
Cell.cass+=dt*(Bcass*(-(ilca-2.0*inacass)*acap/(2.0*frdy*vss)+Jrel*vjsr/vss-Jdiff));

Cell.cansr+=dt*(Jup-Jtr*vjsr/vnsr);

double Bcajsr=1.0/(1.0+csqnbar*kmcsqn/pow(kmcsqn+Cell.cajsr,2.0));
Cell.cajsr+=dt*(Bcajsr*(Jtr-Jrel));

if ( iy == 1 && offset_y==0 ) istim = ist; else istim = 0; //protocol 1
	
naiont = ina+inal+INab+ilcana+3*INaK+3*inaca+3*inacass;
kiont = Ito + IKr + IKs + IK1 + ilcak + IKb - 2*INaK + istim;
caiont = ilca+icab+ipca-2*inaca-2*inacass;		
it = naiont + kiont + caiont;

v[ix][iy] += -it*dt;

} 
