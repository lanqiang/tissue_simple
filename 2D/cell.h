#ifndef CELL_H_INCLUDED
#define CELL_H_INCLUDED

//#include "randomGenerate.h"
#include "RectPart2D.h"

struct cell
{
    int cellID;
   // struct RandData *pRD;
    //struct unit *unitsInCell;//no unit in the simple model

    int unitNum_x;
    int unitNum_y;
    int unitNum_z;

    double m,hf,hs,hsp,j,jp;//same with detailed
    double ml,hl,hlp;//same with detailed
    double nai,ki,nass,kss;//same with detailed

    double ap,a,iF,iS,iFp,iSp;//same with detailed
    double xs1,xs2,xrs,xrf,xk1;//same with detailed
    double d,ff,fcaf,fs,fcas,jca,ffp,fcafp;//same with detailed
    

   // double CaMKa_cell;//added by qianglan
   // double nca_print;//added by qianglan, not used in simple but used in detailed model
   // double inacass_dyad;//added by qianglan

    double nca;//same with detailed
    double CaMKt;//same with detailed
    //double *CaMKa;//added by qianglan
    //double *trel;//not used in simple but used in detailed model
    //double *Poryr;//not used in simple but used in detailed model

    //int *nactive;
    //int randcallcount;
	double cai,cass,cansr,cajsr;
    double Jrelnp,Jrelp,soicr;

    long timestep;
    long totalCellSize;
};


struct cell **Allocate2DSubCells(int sub_N_x,int sub_N_y);
void deallocate2DSubCells(struct cell **SubCells);

void InitCellValue(struct cell & Cell);
void releaseCell(struct cell &Cell);

//void generateSeed(struct cell&Cell, int thread_id,int process_id);
void compute_cell_id(struct cell & Cell, int cx, int cy, RectPart2D* partitioner);



#endif // CELL_H_INCLUDED
