#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <omp.h>
#include<cstring>

#include "allocArray.h"
#include "compute_cell.h"
#include "cell.h"
#include "UpdateGhost.h"
#include "postprocess.h"


#include <malloc.h>
#include <stdlib.h>
#include <cstring>



using namespace std;

double t,dt,d_x,Dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=600;

ofstream fout;
ofstream f1out;
ostringstream file;
ostringstream file1;

double ***v, ***vtemp,***dvdt,***dvdtclock,***dtstep;

void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_z, double t, double ist, int bcl, double ***v,double ***dtstep);

int main(int nargs, char** args)
{

	dt = 0.01;
	d_x = 0.5; //dx for voltage
	Dx = 0.2;
	
	tstim = 50;/////////////////////////////////////////////????????this value is not the same as detailed!
	stimtime = 100;
    int beats = 5;
	steps = beats*bcl/dt;
  

	//***************************************************
	Partitioner_t* partitioner;
	int num_procs1=0, num_procs2=0,num_procs3=0;
	int global_N_x,global_N_y,global_N_z;
	int N_x, N_y,N_z;
	int storeStrideX=1,storeStrideY=1,storeStrideZ=1;

	MPI_Init (&nargs, &args);
	if (nargs>1)
        global_N_x = atoi(args[1]);
	if (nargs>2)
        global_N_y = atoi(args[2]);
	if(nargs>3)
	    global_N_z = atoi(args[3]);
	if (nargs>4)
        num_procs1 = atoi(args[4]);
	if (nargs>5)
        num_procs2 = atoi(args[5]);
	if(nargs>6)
	    num_procs3 = atoi(args[6]);
	if(nargs>7)
	    storeStrideX = atoi(args[7]);
	if(nargs>8)
	    storeStrideY = atoi(args[8]);
	if(nargs>9)
	    storeStrideZ = atoi(args[9]);

	MPI_Comm comm=MPI_COMM_WORLD;
	partitioner = Partitioner_construct(comm,global_N_x,global_N_y,global_N_z,
				                        num_procs1,num_procs2,num_procs3);
	N_x = partitioner->sub_N[0];
	N_y = partitioner->sub_N[1];
	N_z = partitioner->sub_N[2];
	
	int offsetX,offsetY,offsetZ;
	offsetX = partitioner->my_offset[0];
	offsetY = partitioner->my_offset[1];
	offsetZ = partitioner->my_offset[2];
	
	int s_ix,s_iy,s_iz;
	int remain;
	int ds;
	remain = offsetX%storeStrideX;
	if(remain==0)
		s_ix=1;
	else
	{
		ds = storeStrideX - remain;
		s_ix = ds+1;
	}	
	remain = offsetY%storeStrideY;
	if(remain==0)
		s_iy=1;
	else
	{
		ds = storeStrideY - remain;
		s_iy = ds+1;
	}	
	remain = offsetZ%storeStrideZ;
	if(remain==0)
		s_iz=1;
	else
	{
		ds = storeStrideZ - remain;
		s_iz = ds+1;
	}	
	
	//***************************************************

	//Files for output
	
	file<<"file_"<<partitioner->mpi_info.my_rank;
	fout.open(file.str().c_str());
	if (partitioner->mpi_info.my_rank == 0)
		f1out.open("cell");
	
	v = allocate_3D_double(N_x,N_y,N_z);
	vtemp = allocate_3D_double(N_x,N_y,N_z);
	memset(vtemp[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdt = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdt[0][0],0,N_x*N_y*N_z*sizeof(double));
	dvdtclock = allocate_3D_double(N_x,N_y,N_z);
	memset(dvdtclock[0][0],0,N_x*N_y*N_z*sizeof(double));
	dtstep = allocate_3D_double(N_x,N_y,N_z);
	memset(dtstep[0][0],0,N_x*N_y*N_z*sizeof(double));
    
    cell ***subcells;
	subcells = Allocate3DSubCells(N_x,N_y,N_z);
    for(int cx = 0; cx < N_x; cx++)
	    for(int cy =0; cy < N_y; cy++)
		    for(int cz = 0;cz < N_z;cz++)
		    	InitCellValue(subcells[cx][cy][cz]);

	for(int cx = 1;cx<N_x-1;cx++)
	    for(int cy = 1;cy<N_y-1;cy++)
			for(int cz =1;cz<N_z-1;cz++)
			{
	//	      	compute_cell_id(subcells[cx][cy][cz], cx,cy,cz, partitioner);//also initialize the data random seed
            	v[cx][cy][cz] = -87.6;
	   	      //printf("cell id is : %d \n", subcells[cx][cy][cz].cellID);
			}
			
	int offset_x = partitioner->my_offset[0];
	int offset_z = partitioner->my_offset[2];

	#pragma omp parallel
	{
		for (int icounter = 0; icounter<steps; icounter++)
		{
			#pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy][iz])>0.2)||(dvdtclock[ix][iy][iz]<20) )
                            {
                                comp_cell(subcells[ix][iy][iz],ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep);
                                dtstep[ix][iy][iz] = 0;
                            }
	
			#pragma omp for
                for (int ix = 1; ix<N_x-1; ix++)
                    for (int iy = 1; iy<N_y-1; iy++)
                        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);

			#pragma omp master
			{
	 			 UpdateGhostValue(v,partitioner);
			}
			#pragma omp barrier

			#pragma omp for
                for (int ix = 0; ix<N_x; ix++)
                    for (int iy = 0; iy<N_y; iy++)
                        for(int iz = 0;iz<N_z;iz++)
                        {
                            dvdt[ix][iy][iz] = (v[ix][iy][iz] - vtemp[ix][iy][iz])/dt;
                            if (dvdt[ix][iy][iz]>1) 
                                dvdtclock[ix][iy][iz] = 0;
                            dtstep[ix][iy][iz] += dt;
                            dvdtclock[ix][iy][iz] += dt;
                            vtemp[ix][iy][iz] = v[ix][iy][iz];
                        }

			#pragma omp single
			{
				if (t>=tstim && t<(tstim+dt))
				{
					stimtime = 0;
					ibeat = ibeat+1;
					tstim = tstim+bcl;
					if (partitioner->mpi_info.my_rank==0)
		 				 cout<<ibeat<<endl;
				}
	
				if (stimtime>=0 && stimtime<=0.5)
					ist = -80;
				else
					ist = 0;

				//	if (ibeat>2) ist = 0;



				if (icounter%1000 == 0) //  && partitioner->is_master_proc())
				{
					for(int ix=s_ix;ix<N_x-1;ix+=storeStrideX)
						for(int iy=s_iy;iy<N_y-1;iy+=storeStrideY)
						{
							for(int iz=s_iz;iz<N_z-1;iz+=storeStrideZ)
							fout << v[ix][iy][iz] << " ";
							fout << endl;
						}	

					if (partitioner->mpi_info.my_rank==0)
					{
						f1out<<t<<"	"<<v[N_x/2][N_y/2][N_z/2]<<"  "<<subcells[N_x/2][N_y/2][N_z/2].cajsr<<endl;
					}

		
				}

				t+=dt;
				stimtime += dt;
			}//end single region
		}//end time loop
	} //end parallel region
	
	fout.close();
	if (partitioner->mpi_info.my_rank == 0)
		f1out.close();
		
	if (partitioner->mpi_info.my_rank == 0)
		VTKOutPut(num_procs1,num_procs2,num_procs3,
			  global_N_x/storeStrideX,global_N_y/storeStrideY,global_N_z/storeStrideZ,steps/1000);


        deallocate_3D_double (v,N_x);
        deallocate_3D_double (vtemp,N_x);
        deallocate_3D_double (dvdt,N_x);
        deallocate_3D_double (dvdtclock,N_x);

        deallocate_3D_double (dtstep,N_x);

        for(int cx = 0; cx < N_x; cx++)
            for(int cy = 0;cy < N_y; cy++)
                for(int cz =0; cz < N_z;cz++)
                    releaseCell(subcells[cx][cy][cz]);

        deallocate3DSubCells(subcells,N_x);
        Partitioner_destruct(partitioner);

	MPI_Finalize ();
	return 0;
}



