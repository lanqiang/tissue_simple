#include <ostream>
#include <istream>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <stdlib.h>
#include "allocArray.h"

using namespace std;

void VTKOutPut(int num_procs_x,int num_procs_y,int num_procs_z,
		int nx,int ny,int nz,int storeSteps)
{
	int totalInputFiles = num_procs_x*num_procs_y*num_procs_z;
	int totalOutputFiles = storeSteps;
	double ***Vdata;
	int *pos;
	Vdata = allocate_3D_double(nx,ny,nz);
	pos = (int *)malloc(totalInputFiles*sizeof(int));	
	for(int p=0;p<totalInputFiles;p++)
		pos[p]=0;

	int sub_nx,sub_ny,sub_nz;
	if((nx%num_procs_x)!=0||(ny%num_procs_y)!=0||(nz%num_procs_z)!=0)
	{
		cout<<"each process should process the same amount of data!"<<endl;
		return;
	}
	sub_nx = nx/num_procs_x;
	sub_ny = ny/num_procs_y;
	sub_nz = nz/num_procs_z;

	int a = num_procs_z*num_procs_y;

	ifstream fin;
	ofstream fVTKout;
	ostringstream finName;
	ostringstream fVTKoutName;


	for(int i=0;i<totalOutputFiles;i++)
	{
		fVTKoutName.str("");
		fVTKoutName.clear();
		fVTKoutName<<"VTK_"<<i<<".vtk";
		fVTKout.open(fVTKoutName.str().c_str());
		fVTKout << "# vtk DataFile Version 3.0\nMarcus Noack Grid\nASCII\nDATASET STRUCTURED_GRID\nDIMENSIONS       "<<nx<<"    "<<ny<<"    "<<nz<<"\nPOINTS"<<"   "<<   nx*ny*nz<<" double\n";
		for(int ix=0;ix<nx;ix++)
			for(int iy=0;iy<ny;iy++)
				for(int iz=0;iz<nz;iz++)
					fVTKout<<ix<<" "<<iy<<" "<<iz<<"\n";
		fVTKout << "POINT_DATA "<<nx*ny*nz<<"\nSCALARS scalars double \nLOOKUP_TABLE default\n";
		
		//read 3D value from input files
		for(int j=0;j<totalInputFiles;j++)
		{
			finName.str("");
			finName.clear();
			finName<<"file_"<<j;
			fin.open(finName.str().c_str(),ifstream::in);
			fin.seekg(pos[j]);
			int offset1,offset2,offset3;
			int num_volum1,num_volum2,num_volum3;
			num_volum1 = j/a;
			num_volum2 = (j%a)/num_procs_z;
			num_volum3 = (j%a)%num_procs_z;
			offset1 = num_volum1*sub_nx;
			offset2 = num_volum2*sub_ny;
			offset3 = num_volum3*sub_nz;
			for(int ix=0;ix<sub_nx;ix++)
				for(int iy=0;iy<sub_ny;iy++)
					for(int iz=0;iz<sub_nz;iz++)
					{
						fin>>Vdata[offset1+ix][offset2+iy][offset3+iz];
					}
			pos[j] = fin.tellg();
			fin.close();
			
		}
	
		//write the 3D value to output files
		for(int ix=0;ix<nx;ix++)
			for(int iy=0;iy<ny;iy++)
				for(int iz=0;iz<nz;iz++)
					fVTKout<<Vdata[ix][iy][iz]<<"\n";
		fVTKout.close();
	}
	free(pos);
	deallocate_3D_double(Vdata,nx);
}
