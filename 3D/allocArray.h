#ifndef ALLOCARRAY_H_INCLUDED
#define ALLOCARRAY_H_INCLUDED



double ** allocate_2D_double ( int Nx, int Ny);

double *** allocate_3D_double(int Nx, int Ny,int Nz);

int *** allocate_3D_int( int Nx, int Ny, int Nz);

double **** allocate_4D_double( int Nx, int Ny, int Nz, int Nt);

int **** allocate_4D_int( int Nx, int Ny, int Nz, int Nt);

void deallocate_2D_double (double **ptr);

void deallocate_3D_double(double ***ptr,int Nx);

void deallocate_3D_int(int ***ptr,int Nx);

void deallocate_4D_int(int ****ptr,int Nx,int Ny);


#endif // ALLOCARRAY_H_INCLUDED
