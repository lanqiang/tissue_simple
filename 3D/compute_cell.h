#ifndef COMPUTE_CELL_H_INCLUDED
#define COMPUTE_CELL_H_INCLUDED

#include "cell.h"



void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_z, double t, double ist, int bcl, double ***v,double ***dtstep);


#endif // COMPUTE_CELL_H_INCLUDED
