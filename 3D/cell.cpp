#include "cell.h"
#include <cstring>
#include <stdlib.h>

struct cell ***Allocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z)
{
    struct cell ***SubCells;
    struct cell *cells = (struct cell*)malloc(sub_N_x*sub_N_y*sub_N_z*sizeof(struct cell));
    SubCells = (struct cell***)malloc(sub_N_x*sizeof(struct cell**));
    for(int ix=0; ix<sub_N_x; ix++)
    {
        SubCells[ix] = (struct cell**)malloc(sub_N_y*sizeof(struct cell*));
        for(int iy=0;iy<sub_N_y;iy++)
	     SubCells[ix][iy] = &(cells[ix*sub_N_y*sub_N_z+iy*sub_N_z]);
    }
    return SubCells;

}

void deallocate3DSubCells(struct cell ***SubCells,int Nx)
{
    free(SubCells[0][0]);
    for(int i=0;i<Nx;i++)
    	free(SubCells[i]);
    free(SubCells);
    return;
}


void InitCellValue(struct cell & Cell)
{
    Cell.nai=6.3;
    Cell.nass = Cell.nai;
    Cell.ki=145;
    Cell.kss = Cell.ki;
    
    Cell.cai = .72e-4;
    Cell.cass = 1e-4;
    Cell.cansr = 1.44;
    Cell.cajsr = 1.44;
    Cell.CaMKt = 0.009;

    Cell.m=0.01;
    Cell.hf=0.7;
    Cell.hs=0.7;
    Cell.j=0.7;
    Cell.hsp=0.45;
    Cell.jp=.7;
	
    Cell.ml=0.0002;
    Cell.hl=0.5;
    Cell.hlp=0.27;

    Cell.a=0.001;
    Cell.iF=1;
    Cell.iS=.76;
    Cell.ap=.0005;
    Cell.iFp=1;
    Cell.iSp=.8;

    Cell.d=0;
    Cell.ff=1;
    Cell.fs=.91;
    Cell.fcaf=1;
    Cell.fcas=1;
    Cell.jca=1;
    Cell.nca=0.001;
    Cell.ffp=1;
    Cell.fcafp=1;

    Cell.xrf=0;
    Cell.xrs=0.27;
    Cell.xs1=0.25;
    Cell.xs2=0;
    Cell.xk1=1;
    
    Cell.Jrelp=0;
    Cell.Jrelnp=0;
    Cell.soicr=0;  

    //malloc random generator
    //Cell.pRD = (struct RandData *)malloc(sizeof(struct RandData));
    //Cell.randcallcount  = 0;
    Cell.timestep = 0;

    return;
}


void releaseCell(struct cell &Cell)
{
    //free(Cell.pRD);
    return;
}

/*void compute_cell_id(struct cell & Cell, int cx, int cy,int cz, Partitioner_t* partitioner)
{
    int total_N_x = partitioner->N[0];
    int total_N_y = partitioner->N[1];
    int total_N_z = partitioner->N[2];
    int my_offset_x = partitioner->my_offset[0];
    int my_offset_y = partitioner->my_offset[1];
    int my_offset_z = partitioner->my_offset[2];

//    int cell_id = (my_offset_y + cy - 1)*total_N_x + my_offset_x + cx -1;
    int cell_id = (my_offset_x+cx-1)*total_N_z*total_N_y+(my_offset_z+cz-1)*total_N_z+my_offset_y+cy-1;
    Cell.cellID = cell_id+1;//id from 1
    //Cell.cellID = 1;

    /*int my_rank_x = partitioner->my__rank_x();
    int my_rank_y = partitioner->my__rank_y();
    int sub_Nx = partitioner->sub_N_x();
    int sub_Ny = partitioner->sub_N_y();
    int cell_id = my_rank_y*((sub_Ny-2)*total_N_x)+my_rank_x*((sub_Ny-2)*(sub_Nx-2))+(sub_Nx-2)*(cy-1)+(cx-1);
    Cell.cellID = cell_id;*/
    //Cell.pRD->myrandomseed = -Cell.cellID;
    //Cell.pRD->iy = 0;
    //Cell.pRD->randcallcount = 0;*/

/*    Cell.totalCellSize = total_N_x*total_N_y*total_N_z;


    return;

}*/

