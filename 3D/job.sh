#!/bin/bash

#SBATCH --job-name=tissue_simple
#SBATCH --time=02:10:00
#SBATCH --account=nn2849k
#SBATCH --mem-per-cpu=2G
#SBATCH --output=./Output_File.out

#SBATCH --ntasks=8
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --cpus-per-task=16

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
time mpirun -np 8 ./tissueSimple 64 64 64 2 2 2 1 1 1
