#include "cell.h"
#include <cstring>
#include <stdlib.h>

struct cell **Allocate2DSubCells(int sub_N_x,int sub_N_y)
{
    struct cell **SubCells;
    struct cell *cells = (struct cell*)malloc(sub_N_x*sub_N_y*sizeof(struct cell));
    SubCells = (struct cell**)malloc(sub_N_x*sizeof(struct cell*));
    for(int i=0; i<sub_N_x; i++)
        SubCells[i] = &(cells[i*sub_N_y]);
    return SubCells;

}

void deallocate2DSubCells(struct cell **SubCells)
{
    free(SubCells[0]);
    free(SubCells);
    return;
}


void InitCellValue(struct cell & Cell)
{
    Cell.nai=6.3;
    Cell.nass = Cell.nai;
    Cell.ki=145;
    Cell.kss = Cell.ki;
    
    Cell.cai = .72e-4;
    Cell.cass = 1e-4;
    Cell.cansr = 1.44;
    Cell.cajsr = 1.44;
    Cell.CaMKt = 0.009;

    Cell.m=0.01;
    Cell.hf=0.7;
    Cell.hs=0.7;
    Cell.j=0.7;
    Cell.hsp=0.45;
    Cell.jp=.7;
	
    Cell.ml=0.0002;
    Cell.hl=0.5;
    Cell.hlp=0.27;

    Cell.a=0.001;
    Cell.iF=1;
    Cell.iS=.76;
    Cell.ap=.0005;
    Cell.iFp=1;
    Cell.iSp=.8;

    Cell.d=0;
    Cell.ff=1;
    Cell.fs=.91;
    Cell.fcaf=1;
    Cell.fcas=1;
    Cell.jca=1;
    Cell.nca=0.001;
    Cell.ffp=1;
    Cell.fcafp=1;

    Cell.xrf=0;
    Cell.xrs=0.27;
    Cell.xs1=0.25;
    Cell.xs2=0;
    Cell.xk1=1;
    
    Cell.Jrelp=0;
    Cell.Jrelnp=0;
    Cell.soicr=0;  

    //malloc random generator
    Cell.pRD = (struct RandData *)malloc(sizeof(struct RandData));
    //Cell.randcallcount  = 0;
    Cell.timestep = 0;

    return;
}


void releaseCell(struct cell &Cell)
{
    free(Cell.pRD);
    return;
}

void compute_cell_id(struct cell & Cell, int cx, int cy, RectPart2D* partitioner)
{
    int total_N_x =  partitioner->total_N_x();
    int total_N_y = partitioner->total_N_y();
    int my_offset_x = partitioner->offset_x();
    int my_offset_y = partitioner->offset_y();

    int cell_id = (my_offset_y + cy - 1)*total_N_x + my_offset_x + cx -1;
    Cell.cellID = cell_id+1;//id from 1
    //Cell.cellID = 1;
    /*int my_rank_x = partitioner->my__rank_x();
    int my_rank_y = partitioner->my__rank_y();
    int sub_Nx = partitioner->sub_N_x();
    int sub_Ny = partitioner->sub_N_y();
    int cell_id = my_rank_y*((sub_Ny-2)*total_N_x)+my_rank_x*((sub_Ny-2)*(sub_Nx-2))+(sub_Nx-2)*(cy-1)+(cx-1);
    Cell.cellID = cell_id;*/
    Cell.pRD->myrandomseed = -Cell.cellID;
    Cell.pRD->iy = 0;
    Cell.pRD->randcallcount = 0;
    Cell.totalCellSize = total_N_x*total_N_y;
    return;

}

