#include "RectPart2D.h"
#include <cmath>
#include <malloc.h>

RectPart2D::~RectPart2D ()
{
  if (upper_out_msg)
    free (upper_out_msg);
  if (upper_in_msg)
    free (upper_in_msg);
  if (lower_out_msg)
    free (lower_out_msg);
  if (lower_in_msg)
    free (lower_in_msg);
}

void RectPart2D::preparation (int N1, int N2, int num_procs1, int num_procs2)
{
  int i,j;

  MPI_Comm_size (MPI_COMM_WORLD, &total_MPI_procs);
  MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);

  if (total_MPI_procs != (num_procs1*num_procs2)) {
    i = sqrt(total_MPI_procs);
    for (j=i; j>=1; j--)
      if ((total_MPI_procs%j) == 0) {
	num_procs2 = j;
	num_procs1 = total_MPI_procs/num_procs2;
	j = 0;
      }
  }

  num_procs_x = num_procs1;
  num_procs_y = num_procs2;

  my_rank_x = my_rank % num_procs_x;
  my_rank_y = my_rank / num_procs_x;

  left_neighbor_ID = (my_rank_x>0) ? my_rank_y*num_procs_x+my_rank_x-1 : -1;
  right_neighbor_ID = (my_rank_x<num_procs_x-1) ? my_rank_y*num_procs_x+my_rank_x+1 : -1;
  lower_neighbor_ID = (my_rank_y>0) ? (my_rank_y-1)*num_procs_x+my_rank_x : -1;
  upper_neighbor_ID = (my_rank_y<num_procs_y-1) ? (my_rank_y+1)*num_procs_x+my_rank_x : -1;

  N_x = N1;
  N_y = N2;

  i = my_rank_x*(N_x)/num_procs_x;
  j = (my_rank_x+1)*(N_x)/num_procs_x;
  my_offset_x = i;
  sub_Nx = j-i+2;

  i = my_rank_y*(N_y)/num_procs_y;
  j = (my_rank_y+1)*(N_y)/num_procs_y;
  my_offset_y = i;
  sub_Ny = j-i+2;

  printf("<%d> (%d,%d) [%d,%d] [%d,%d]\n",my_rank,my_rank_x,my_rank_y,
	 sub_Nx,sub_Ny,my_offset_x,my_offset_y);

  if (upper_neighbor_ID>=0) {
    upper_out_msg = (double*)malloc((sub_Nx-2)*sizeof(double));
    upper_in_msg = (double*)malloc((sub_Nx-2)*sizeof(double));
  }

  if (lower_neighbor_ID>=0) {
    lower_out_msg = (double*)malloc((sub_Nx-2)*sizeof(double));
    lower_in_msg = (double*)malloc((sub_Nx-2)*sizeof(double));
  }
}

void RectPart2D::start_communication (double **array2D)
{
  if (left_neighbor_ID>=0) {
    left_out_msg = &(array2D[1][1]);
    left_in_msg = &(array2D[0][1]);
    MPI_Isend (left_out_msg,sub_Ny-2,MPI_DOUBLE,left_neighbor_ID,101,
	       MPI_COMM_WORLD,&left_out_request);
    MPI_Irecv (left_in_msg,sub_Ny-2,MPI_DOUBLE,left_neighbor_ID,102,
	       MPI_COMM_WORLD,&left_in_request);
  }

  if (right_neighbor_ID>=0) {
    right_out_msg = &(array2D[sub_Nx-2][1]);
    right_in_msg = &(array2D[sub_Nx-1][1]);
    MPI_Isend (right_out_msg,sub_Ny-2,MPI_DOUBLE,right_neighbor_ID,102,
	       MPI_COMM_WORLD,&right_out_request);
    MPI_Irecv (right_in_msg,sub_Ny-2,MPI_DOUBLE,right_neighbor_ID,101,
	       MPI_COMM_WORLD,&right_in_request);
  }

  if (lower_neighbor_ID>=0) {
    for (int i=1; i<=sub_Nx-2; i++)
      lower_out_msg[i-1] = array2D[i][1];
    MPI_Isend (lower_out_msg,sub_Nx-2,MPI_DOUBLE,lower_neighbor_ID,103,
	       MPI_COMM_WORLD,&lower_out_request);
    MPI_Irecv (lower_in_msg,sub_Nx-2,MPI_DOUBLE,lower_neighbor_ID,104,
	       MPI_COMM_WORLD,&lower_in_request);
  }

  if (upper_neighbor_ID>=0) {
    for (int i=1; i<=sub_Nx-2; i++)
      upper_out_msg[i-1] = array2D[i][sub_Ny-2];
    MPI_Isend (upper_out_msg,sub_Nx-2,MPI_DOUBLE,upper_neighbor_ID,104,
	       MPI_COMM_WORLD,&upper_out_request);
    MPI_Irecv (upper_in_msg,sub_Nx-2,MPI_DOUBLE,upper_neighbor_ID,103,
	       MPI_COMM_WORLD,&upper_in_request);
  }
}

void RectPart2D::finish_communication (double **array2D)
{
  if (left_neighbor_ID>=0) {
    MPI_Wait (&left_out_request,&status);
    MPI_Wait (&left_in_request,&status);
  }

  if (right_neighbor_ID>=0) {
    MPI_Wait (&right_out_request,&status);
    MPI_Wait (&right_in_request,&status);
  }

  if (lower_neighbor_ID>=0) {
    MPI_Wait (&lower_out_request,&status);
    MPI_Wait (&lower_in_request,&status);
    for (int i=1; i<=sub_Nx-2; i++)
      array2D[i][0] = lower_in_msg[i-1];
  }

  if (upper_neighbor_ID>=0) {
    MPI_Wait (&upper_out_request,&status);
    MPI_Wait (&upper_in_request,&status);
    for (int i=1; i<=sub_Nx-2; i++)
      array2D[i][sub_Ny-1] = upper_in_msg[i-1];
  }
}
