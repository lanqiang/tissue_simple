#ifndef RANDOMGENERATE_H_INCLUDED
#define RANDOMGENERATE_H_INCLUDED

#define NTAB 32
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836

#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-12
#define RNMX (1.0-EPS)



struct RandData
{
    long myrandomseed;
    long iy;
    long iv[NTAB];
    double padding[16];
    int randcallcount;
    double randomDataGenerate();
};

#endif // RANDOMGENERATE_H_INCLUDED
