
#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <omp.h>

#include "allocArray.h"
#include "compute_cell.h"
#include "cell.h"

//#include "RectPart2D.h"
#include <malloc.h>
#include <stdlib.h>
#include <cstring>
#define global_N_x 300
#define global_N_y 300
#define beats 5

using namespace std;

double t,dt,d_x,Dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=600;

ofstream fout;
ofstream f1out;
ostringstream file;

double **v, **vtemp,**dvdt,**dvdtclock,**dtstep;

void comp_cell(int ix, int iy, int offset_x, int offset_y);



int main(int nargs, char** args)
{

	dt = 0.01;
	d_x = 0.5; //dx for voltage
	Dx = 0.2;
	
	tstim = 50;
	stimtime = 100;

	steps = beats*bcl/dt;


	//***************************************************
	RectPart2D* partitioner;
	int num_procs1=0, num_procs2=0;
	int N_x, N_y;

	MPI_Init (&nargs, &args);
	if (nargs>1)
	  num_procs1 = atoi(args[1]);
	if (nargs>2)
	  num_procs2 = atoi(args[2]);

	partitioner = new RectPart2D(global_N_x,global_N_y,
				     num_procs1,num_procs2);
	N_x = partitioner->sub_N_x();
	N_y = partitioner->sub_N_y();
	
	//***************************************************

	//Files for output
	
	file<<"file_"<<partitioner->my__rank();
	fout.open(file.str().c_str());
	if (partitioner->my__rank() == 0)
		f1out.open("cell");
	
	v = allocate_array2D(N_x,N_y);
    vtemp = allocate_array2D(N_x,N_y);
    memset(vtemp[0],0,N_x*N_y*sizeof(double));
    dvdt = allocate_array2D(N_x,N_y);
    memset(dvdt[0],0,N_x*N_y*sizeof(double));
    dvdtclock = allocate_array2D(N_x,N_y);
    memset(dvdtclock[0],0,N_x*N_y*sizeof(double));
    dtstep = allocate_array2D(N_x,N_y);
    memset(dtstep[0],0,N_x*N_y*sizeof(double));
    
    cell **subcells;
    subcells = Allocate2DSubCells(N_x,N_y);
    for(int cx = 1; cx < N_x-1; cx++)
        for(int cy = 1;cy < N_y-1; cy++)
        {
            InitCellValue(subcells[cx][cy]);
            compute_cell_id(subcells[cx][cy], cx,cy, partitioner);//also initialize the data random seed
	        printf("cell id is : %d \n", subcells[cx][cy].cellID);
            v[cx][cy] = -87.6;
        }



	#pragma omp parallel
	{


	for (int icounter = 0; icounter<steps; icounter++)
	{

	#pragma omp for
	for (int ix = 1; ix<N_x-1; ix++) 
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{
			if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy])>0.2)||(dvdtclock[ix][iy]<20) )
			{
			  	comp_cell(subcells[ix][iy],ix,iy,partitioner->offset_x(),
				partitioner->offset_y(), t, ist, bcl, v, dtstep);
				dtstep[ix][iy] = 0;
			}

		}
	}
	
	#pragma omp for
	for (int ix = 1; ix<N_x-1; ix++)
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{	
			v[ix][iy] += dt*Dx*(vtemp[ix+1][iy] + vtemp[ix-1][iy] + vtemp[ix][iy-1] + vtemp[ix][iy+1] -4*vtemp[ix][iy])
					/(d_x*d_x);
		}
	}

	#pragma omp master
	{
	  int i;
	  partitioner->start_communication(v);

	  // -------- treatment of boundary conditions start ----
	  if (!partitioner->has_lower_neighbor())
	    for (i=1; i<N_x-1; i++)
	      v[i][0] = v[i][1];

	  if (!partitioner->has_upper_neighbor())
	    for (i=1; i<N_x-1; i++)
	      v[i][N_y-1] = v[i][N_y-2];

	  if (!partitioner->has_left_neighbor())
	    for (i=1; i<N_y-1; i++)
	      v[0][i] = v[1][i];

	  if (!partitioner->has_right_neighbor())
	    for (i=1; i<N_y-1; i++)
	      v[N_x-1][i] = v[N_x-2][i];
	  // -------- treatment of boundary conditions end ----

	  partitioner->finish_communication(v);
	}
	#pragma omp barrier

	#pragma omp for
	for (int ix = 0; ix<N_x; ix++)
	{
		for (int iy = 0; iy<N_y; iy++)
		{
			dvdt[ix][iy] = (v[ix][iy] - vtemp[ix][iy])/dt;
			if (dvdt[ix][iy]>1) dvdtclock[ix][iy] = 0;
			dtstep[ix][iy] += dt;
			dvdtclock[ix][iy] += dt;
			vtemp[ix][iy] = v[ix][iy];
		}
	}

	#pragma omp single
	{
	if (t>=tstim && t<(tstim+dt))
	{
		stimtime = 0;
		ibeat = ibeat+1;
		tstim = tstim+bcl;
		if (partitioner->is_master_proc())
		  cout<<ibeat<<endl;
	}
	
	if (stimtime>=0 && stimtime<=0.5)
		ist = -80;
	else
		ist = 0;

//	if (ibeat>2) ist = 0;



	if (icounter%1000 == 0) //  && partitioner->is_master_proc())
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{
			for (int ix = 1; ix<N_x-1; ix++)
			{
				fout<<v[ix][iy]<<"	";
				
			}
			fout<<endl;
		}

		if (partitioner->is_master_proc())
		{
			f1out<<t<<"	"<<v[N_x/2][N_y/2]<<subcells[N_x/2][N_y/2].cajsr<<endl;
		}

		
	}

	t+=dt;
	stimtime += dt;
	}//end single region
	}//end time loop
	} //end parallel region

	MPI_Finalize ();
	return 0;
}



